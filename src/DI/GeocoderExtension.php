<?php

namespace Atreo\Geocoder\DI;

use Nette\DI\CompilerExtension;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class GeocoderExtension extends CompilerExtension
{

	public function loadConfiguration()
	{
		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('seznamGeocoder'))
			->setClass('Atreo\Geocoder\SeznamGeocoder');

	}

}
