<?php

namespace Atreo\Geocoder;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Kdyby\Doctrine\Entities\MagicAccessors;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 * @ORM\Entity()
 *
 * @property string $phrase
 * @property float $lat
 * @property float $lng
 */
class GeocoderCache
{

	use Identifier;
	use MagicAccessors;

	/**
	 * @ORM\Column(type="string", unique=TRUE)
	 * @var string
	 */
	protected $phrase;

	/**
	 * @ORM\Column(type="float")
	 * @var float
	 */
	protected $lat;

	/**
	 * @ORM\Column(type="float")
	 * @var float
	 */
	protected $lng;

}
