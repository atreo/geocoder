<?php

namespace Atreo\Geocoder\Queries;

use Kdyby\Doctrine\QueryObject;
use Kdyby\Persistence\Queryable;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
class GeocoderCacheQuery extends QueryObject
{

	/**
	 * @var string|NULL
	 */
	private $phrase;



	/**
	 * @param string $phrase
	 */
	public function __construct($phrase)
	{
		$this->phrase = $phrase;
	}



	/**
	 * @param \Kdyby\Persistence\Queryable $repository
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	protected function doCreateQuery(Queryable $repository)
	{
		return $repository->createQueryBuilder('gc')
			->where('gc.phrase = :phrase')->setParameter('phrase', $this->phrase);
	}

}
