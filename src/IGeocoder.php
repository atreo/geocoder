<?php

namespace Atreo\Geocoder;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 */
interface IGeocoder
{

	public function geocodeAddress($city, $street);

	/**
	 * @return Coordinates
	 */
	public function getCoordinates();

	public function getIsSingleResult();

}
