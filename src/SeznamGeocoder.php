<?php

namespace Atreo\Geocoder;

use Kdyby\Curl\Request;
use Kdyby\Doctrine\EntityManager;
use Atreo\Geocoder\Queries\GeocoderCacheQuery;
use Nette\Object;
use Nette\Utils\Strings;



/**
 * @author Martin Pánek <kontakt@martinpanek.cz>
 * @property-read Coordinates $coordinates
 * @property-read bool $isResultValid
 * @property-read bool $isSingleResult
 */
class SeznamGeocoder extends Object implements IGeocoder
{

	const URL = 'http://api4.mapy.cz/geocode?query=__ADDRESS__';

	/**
	 * @var \Kdyby\Doctrine\EntityManager
	 */
	private $em;

	/**
	 * @var Coordinates
	 */
	private $coordinates;

	/**
	 * @var bool
	 */
	private $isResultValid = FALSE;

	/**
	 * @var bool
	 */
	private $isSingleResult = FALSE;



	/**
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}



	/**
	 * @param string $city
	 * @param string $street
	 */
	public function geocodeAddress($city, $street = '')
	{
		$this->coordinates = NULL;
		if (Strings::length($street) > 0) {
			$city .= ', ' . $street;
		}

		$city .= ', Česká republika';

		$geocoderCache = $this->em->getRepository(GeocoderCache::getClassName())->fetchOne(new GeocoderCacheQuery($city));
		if ($geocoderCache) {
			$this->coordinates = new Coordinates($geocoderCache->lat, $geocoderCache->lng);
			$this->isResultValid = TRUE;
			$this->isSingleResult = TRUE;
			return;
		}

		$address = Strings::replace($city, '# #', '+');
		$url = Strings::replace(self::URL, '#__ADDRESS__#', $address);

		$request = new Request($url);
		$xml = new \SimpleXMLElement($request->get()->getResponse());

		$this->isResultValid = FALSE;
		$this->isSingleResult = FALSE;
		if ($this->validateResult($xml)) {
			$this->isResultValid = TRUE;

			$this->coordinates = new Coordinates($this->extractLatitude($xml), $this->extractLongitude($xml));

			if (count($xml->point[0]->item) == 1) {
				$this->isSingleResult = TRUE;
			}

			$geocoderCache = new GeocoderCache();
			$this->em->persist($geocoderCache);
			$geocoderCache->phrase = $city;
			$geocoderCache->lat = $this->coordinates->latitude;
			$geocoderCache->lng = $this->coordinates->longitude;
			$this->em->flush();
		}
	}



	/**
	 * @param \SimpleXMLElement $results
	 * @return float|NULL
	 */
	public function extractLatitude(\SimpleXMLElement $results)
	{
		if (!$this->isResultValid) {
			$this->latitude = NULL;
		}

		return (float) $results->point[0]->item['y'];
	}



	/**
	 * @param \SimpleXMLElement $results
	 * @return float|NULL
	 */
	public function extractLongitude(\SimpleXMLElement $results)
	{
		if (!$this->isResultValid) {
			$this->longitude = NULL;
		}

		return (float) $results->point[0]->item['x'];
	}



	/**
	 * @return Coordinates
	 */
	public function getCoordinates()
	{
		return $this->coordinates;
	}



	/**
	 * @return bool
	 */
	public function getIsSingleResult()
	{
		return $this->isSingleResult;
	}



	/**
	 * @return boolean
	 */
	public function isIsResultValid()
	{
		return $this->isResultValid;
	}



	/**
	 * @param \SimpleXMLElement $results
	 * @return bool
	 */
	private function validateResult(\SimpleXMLElement $results)
	{
		return (is_object($results) && $results->point[0]->item['x'] && $results->point[0]->item['y']); //  || $this->results->status == 'ZERO_RESULTS'
	}

}
